---
layout: page-fullwidth
classes: wide
title: "Coming soon: Environment90m"
meta_title: "Env90m"
permalink: "/environment90m/environment90m_layers"
header:
   image_fullwidth: "hydrography90m/dem_streamOrder1.jpg"
---


We are currently preparing a new dataset called Environment90m.

For each of the x million sub-catchments present in
[Hydrography90m data](/hydrography90m/hydrography90m_layers),
it provides detailed data on soil condition, bioclimatic variables,
land cover, and many more.

Just like the Hydrography90m layers, it will be openly available for
download, and easy to download using the dedicated functions in the
R package [hydrographr](/hydrographr/).

#### Stay tuned!

<style>
	table, th, td {border: 0px solid black; background-color: white;}

	.tileDownloadBoundsTitle {
		padding-bottom: 5px;
	}
	.mapTileDownloadContainer {
		width: 685px;
		height: 267px;
		display: block;
		position: relative;
	}
	.mapTileDownloadBaseLayer {
		position: absolute;
		left:0;
		right:0;
	}
.tile {
  width:34px;
  height:34px;
  border:1px solid #606060;
  display:block;
  position:absolute;
  background-color: rgba(128,128,128,0.01);
}

.tile:hover {
  border:2px solid red;
  z-index:2;
  background-color: rgba(128,128,128,0.01);
}

.tile.selected {
  border:1px solid red;
  opacity: 0.6;
  z-index:2;
  background-color: red;
}

#tilepaths a {
  font-family: monospace;
  font-size: 10px;
}

#tilepaths a:link {
  text-decoration: none;
}

#tilepaths a:visited {
  text-decoration: none;
}

#tilepaths a:hover {
  text-decoration: underline;
}

#tilepaths a:active {
  text-decoration: underline;
}

div.anchorcontainer {
	position: relative;
	height:20px;
}


th.th1 {
	font-size: 25px;
}
th.th2 {
	font-size: 18px;
}


code {
  font-weight: bold;
}

</style>
