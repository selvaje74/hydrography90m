---
layout              : page
title               : "Please cite us!"
meta_title          : "Citation"
permalink           : "/citation/"
---

If you use our data, please cite our publication(s):

### Hydrography90m

Amatulli, G., Garcia Marquez, J.R., Sethi, T., Kiesel, J., Grigoropoulou, A., Üblacker, M., Shen, L., Domisch, S. (2022). Hydrography90m: A new high-resolution global hydrographic dataset. Earth System Science Data, 14, 4525–4550, [doi:10.5194/essd-14-4525-2022](doi:10.5194/essd-14-4525-2022).



### R package hydrographr

Schürz, M., Grigoropoulou, A., Garcia Marquez, J.R., Tomiczek, T., Floury, M., Schürz, C., Amatulli, G., Grossart, H.-P., Domisch, S. (2023). hydrographr: an R package for scalable hydrographic data processing. Methods in Ecology and Evolution, [doi:10.1111/2041-210X.14226](doi:10.1111/2041-210X.14226).



### Environment90m (in prep)

Garcia Marquez, J., Amatulli, G., Grigoropoulou, A., Schürz, M., Tomiczek, T., Buurman, M., Bremerich, V., Bego, K. and Domisch, S.: Global datasets of aggregated environmental variables at the sub-catchment scale for freshwater biodiversity modeling, in prep.

(Please contact the authors for more up-to-date citation info)


