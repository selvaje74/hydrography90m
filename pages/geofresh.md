---
layout              : page
title               : "GeoFRESH"
meta_title          : "GeoFRESH"
permalink           : "/geofresh/"
---


GeoFRESH is another member of our family of tools that helps researchers to
interact with the Hydrography90m data!

### Upload and analyze your data

<br />

On the GeoFRESH platform, you can...

* upload your data points,
* see them on a map,
* match them with the nearest stream segment of the Hydrography90m dataset,
* delineate upstream catchments for your points,
* extract a suite of environmental attributes across the catchment,
* identify intersection points between stream network and lakes,
* and download the data for further analyses.

### Check it out at [https://geofresh.org](https://geofresh.org)

